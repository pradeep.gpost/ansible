# Reference shell commands to install docker, docker-compose and run docker compose file on EC2 instance.
# Install docker
sudo yum update -y
sudo yum install -y docker

# Install docker-compose
sudo curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

sudo systemctl start docker

sudo usermod -aG docker ec2-user
# Need to reconnect to ssh to see above change.

# Copy docker-compose.yaml file from local to remote server

# docker login on remote server
docker login -u <username> -p <password>

# Start contianers using docker-compose
docker-compose -f docker-compose.yaml up 
OR
docker-compose up 

# Verify containers are running on remote server 
docker ps 



