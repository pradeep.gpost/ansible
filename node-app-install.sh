# This file is used as reference commands to execute ansible playbook.
# Linux commands to execute nodeapp on ubuntu
apt-get update
apt install nodejs

adduser pradeep
usermod -a -G admin pradeep

su - pradeep

# copy node-app tar.gz file to remote server user home directory (/home/pradeep) and untar the file (package folder)

# cd /home/pradeep/package
# Run below commands in above directory
 npm install  
 node server.js

 # Ensure app is running
 ps aux | grep node 