# Summary
Ansible yaml files to deploy node application on remote server, to deploy nexus software on remote server, to run containers from docker-compose file on remote server, sequential execution of ansible file after terraform scripts complete or create EC2 instance are demonstrated in the master repository. Shell scripts are also added for reference to prepare the ansible files.

# Files and their usage
deploy-node-app.yaml: Install node and npm on remote server, create a service user, copy and unzip artifact file of node application to remote server, run the application, verify if the application is running.

deploy-nexus.yaml: Install pre-requsite softwares like java 8 on remote server, download and unzip the software, create service user and change ownerships of unzipped folders, start nexus application and verify the running state.

deploy-docker-ec2-user.yaml: Install docker, docker-compose, copy docker compose file to remote server and run the containers from docker compose file which include java gradle application, mysql database and phpmyadmin ui.

deploy-docker-new-user.yaml: The ansible file is made little generic. Instead of using ec2-user to docker login,copy file to remote, docker-compose, a linux user 'pradeep' is used. Terraform files that create EC2 instance before executing this file in sequence are at https://gitlab.com/pradeep.gpost/terraform-repo/-/tree/terraform-ansible-docker 

deploy-nginx.yaml: Install and start nginx server.

hosts: Inventory file. node_server, nexus_server group ips represent Linux Ubuntu servers from Digital ocean's droplets. docker_server group ip represent EC2 instance.

project-vars: Variables file that is used in deploy-node-app.yaml file.

ansible.cfg: Ansible configuration file for project.

node-app-install.sh: Shell reference commands to deploy node application.

nexus-install.sh: Shell reference commands to deploy nexus software.

ec2-docker-install.sh: Shell reference commands to deploy docker containers using docker compose.


# To ssh and ping to remote servers, -m is module ping, all is for all ips or all servers. Can also ping specific server ip or server group.
ansible all -i hosts -m ping   # all means all ips or groups in inventory file hosts

ansible node_app_droplet -i hosts -m ping  # node_app_droplet is a group in inventory file hosts

ansible 139.59.12.196 -i hosts -m ping    # 139.59.12.196 is an ip in inventory file hosts

# Run playbook 
ansible-playbook deploy-nexus.yaml

# Run playbook if inventory is not set to hosts file in ansible.cfg file
ansible-playbook -i hosts deploy-nexus.yaml 

# To pass variable values, can use -e instead of --extra-vars
ansible-playbook deploy-node-app.yaml --extra-vars "version=1.0.0 location=/Users/pradeepch/Desktop/Devops-course2/
ansible/deploy-apps/node-project/app linux_user=pradeep"
