# Reference commands to install nexus on linux ubuntu
apt update
apt install openjdk-8-jre-headless # java 8 version is pre-requisite
apt install net-tools # to access netstat commands for in use port checking

cd /opt
wget https://download.sonatype.com/nexus/3/latest-unix.tar.gz 
tar -xzvf latest-unix.tar.gz 
# It usually extracts to two folders . Eg: nexus-3.36.0-01 and sonatype-work
# Rename nexus-3.36.0-01 to nexus 
mv nexus-3.36.0-01 nexus

adduser nexus
chown -R nexus:nexus nexus
chown -R nexus:nexus sonatype-work


vim nexus/bin/nexus.rc 
run_as_user="nexus"
su - nexus
/opt/nexus/bin/nexus start


ps aux | grep nexus
netstat -lnpt 